package net.paoloambrosio.controller;

import net.paoloambrosio.service.MessageRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class UserController {

    @Autowired
    private MessageRepository messageRepository;

    @RequestMapping(value = "/users/{username}", method = RequestMethod.GET)
    public ModelAndView userPage(@PathVariable String username) {
        return new ModelAndView("user", "messages",
                messageRepository.findByAuthor(username));
    }
}
