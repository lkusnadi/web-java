package net.paoloambrosio.service;

import net.paoloambrosio.model.Message;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import java.util.List;

@Repository
public class JpaMessageRepository implements MessageRepository {

    @PersistenceContext
    private EntityManager entityManager;

    @Transactional
    public void save(final Message message) {
        entityManager.persist(message);
    }

    public List<Message> findAll() {
        return entityManager.createQuery("SELECT o FROM Message o",
                Message.class).getResultList();
    }

    public List<Message> findByAuthor(final String username) {
        final TypedQuery<Message> q = entityManager.createQuery(
                "SELECT o FROM Message o WHERE o.author.username = :author",
                Message.class);
        q.setParameter("author", username);
        return q.getResultList();
    }

    public List<Message> findByContent(final String partialText) {
        final TypedQuery<Message> q = entityManager.createQuery(
                "SELECT o FROM Message o WHERE o.content LIKE :likeExpr",
                Message.class);
        q.setParameter("likeExpr", "%" + partialText + "%");
        return q.getResultList();
    }

    @Transactional
    public void deleteAll() {
        entityManager.createQuery("DELETE FROM Message").executeUpdate();
    }
}
