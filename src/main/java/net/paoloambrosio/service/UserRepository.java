package net.paoloambrosio.service;

import net.paoloambrosio.model.User;

import java.util.List;

public interface UserRepository {

    void save(User user);

    List<User> findAll();

    User findByUsername(String username);

    void deleteAll();
}
