<?xml version="1.0" encoding="UTF-8" ?>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title>Squeaker</title>
    <link rel="stylesheet" href="stylesheets/search.css" type="text/css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js" type="text/javascript"></script>
    <script src="javascripts/search.js" type="text/javascript"></script>
</head>
<body>
<form id="search" method="get">
    <fieldset>
        <input type="text" id="query" name="query" autocomplete="off"/>
        <input type="submit" id="submit" value="Search"/>
    </fieldset>

    <ol class="results">
    <#list messages as message>
        <li>${message.content}</li></#list>
    </ol>
</form>

</body>
</html>
