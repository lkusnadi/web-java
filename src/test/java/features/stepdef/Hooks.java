package features.stepdef;

import cucumber.api.java.Before;
import net.paoloambrosio.service.MessageRepository;
import net.paoloambrosio.service.UserRepository;
import org.eclipse.jetty.server.Server;
import org.springframework.beans.factory.annotation.Autowired;
import utils.page.WebDriverFactory;

import java.util.concurrent.TimeUnit;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class Hooks {

    @Autowired
    private Server server;

    @Autowired
    private WebDriverFactory webDriverFactory;

    @Autowired
    private MessageRepository messageRepository;

    @Autowired
    private UserRepository userRepository;

    @Before
    public void checkServerIsRunning() {
        assertTrue(server.isStarted());
    }

    @Before
    public void clearData() {
        messageRepository.deleteAll();
        assertEquals(0, messageRepository.findAll().size());
        userRepository.deleteAll();
        assertEquals(0, userRepository.findAll().size());
    }

    @Before("@javascript")
    public void setJavascriptWebDriver() {
        webDriverFactory.selectJavascriptDriver();
        webDriverFactory.getCurrentWebDriver().manage().timeouts().implicitlyWait(1000, TimeUnit.MILLISECONDS);
    }

    @Before("~@javascript")
    public void setNoJavascriptWebDriver() {
        webDriverFactory.selectDefaultDriver();
    }

}
