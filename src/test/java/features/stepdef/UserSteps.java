package features.stepdef;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.When;
import net.paoloambrosio.model.Message;
import net.paoloambrosio.model.User;
import net.paoloambrosio.service.MessageRepository;
import net.paoloambrosio.service.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import utils.page.UserPage;

import java.util.List;

import static com.natpryce.makeiteasy.MakeItEasy.*;
import static utils.databuilder.MessageMaker.*;
import static utils.databuilder.UserMaker.TestUser;

@SuppressWarnings("unchecked")
public class UserSteps {

    @Autowired
    private UserPage userPage;

    @Autowired
    private MessageRepository messageRepository;

    @Autowired
    private UserRepository userRepository;

    private User user;

    @Given("^there is a User$")
    public void thereIsAuser() {
        user = make(a(TestUser));
        userRepository.save(user);
    }

    @Given("^the User has posted the message \"([^\"]*)\"$")
    public void theUserHasPostedTheMessage(final String msg) {
        messageRepository.save(make(a(TestMessage, with(author, user), with(content, msg))));
    }

    @Given("^a User has posted the following messages:$")
    public void aUserHasPostedTheFollowingMessage(List<Message> messages) {
        thereIsAuser();
        for (Message m : messages) {
            m.setAuthor(user);
            messageRepository.save(m);
        }
    }

    @When("^I visit the page for the User$")
    public void visitThePageForTheUser() {
        userPage.visitFor(user);
    }

}
