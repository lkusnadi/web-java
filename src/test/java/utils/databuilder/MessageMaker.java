package utils.databuilder;

import com.natpryce.makeiteasy.Instantiator;
import com.natpryce.makeiteasy.Property;
import com.natpryce.makeiteasy.PropertyLookup;
import net.paoloambrosio.model.Message;
import net.paoloambrosio.model.User;

import java.util.UUID;

public class MessageMaker {

    public static final Property<Message, User> author = new Property<Message, User>();
    public static final Property<Message, String> content = new Property<Message, String>();

    public static final Instantiator<Message> TestMessage = new Instantiator<Message>() {
        @Override
        public Message instantiate(PropertyLookup<Message> lookup) {
            Message message = new Message();
            message.setAuthor(lookup.valueOf(author, (User) null));
            message.setContent(lookup.valueOf(content, UUID.randomUUID().toString()));
            return message;
        }
    };

}
