package utils.page;

import org.openqa.selenium.WebDriver;
import org.springframework.beans.factory.annotation.Autowired;

public class BrowserPage {

    protected final String BASE_URL = "http://localhost:8080/test";

    @Autowired
    private WebDriverFactory webDriverFactory;

    protected WebDriver webDriver() {
        return webDriverFactory.getCurrentWebDriver();
    }

}
