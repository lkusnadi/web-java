package utils.page;

import org.openqa.selenium.WebDriver;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.stereotype.Component;

@Component
public class WebDriverFactory implements ApplicationContextAware {

    private ApplicationContext applicationContext;

    private WebDriver currentWebDriver;

    @Override
    public void setApplicationContext(ApplicationContext applicationContext) {
        this.applicationContext = applicationContext;
    }

    public void selectJavascriptDriver() {
        currentWebDriver = getJsWebDriver();
    }

    public void selectDefaultDriver() {
        currentWebDriver = getDefaultWebDriver();
    }

    public WebDriver getCurrentWebDriver() {
        return currentWebDriver;
    }

    private WebDriver getJsWebDriver() {
        return applicationContext.getBean("javascript-webdriver", WebDriver.class);
    }

    private WebDriver getDefaultWebDriver() {
        return applicationContext.getBean("default-webdriver", WebDriver.class);
    }
}
